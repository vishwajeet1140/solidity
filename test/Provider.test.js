var Provider = artifacts.require("./Provider.sol");
const assert = require('assert');
//Test Suite

contract('Provider',function(accounts){

    let appInstance;
    var owner = accounts[0];
    //Setup Mock Provider Attributes
    var providerFirstName = "John";
    var providerLastName = "Doe";
    var providerNpi = "111222333";
    var providerSuffix = "Dr.";
    var providerIsVerified = "true";

    var provider1FirstName = "Jane";
    var provider1LastName = "Doe";
    var provider1Npi = "444555666";
    var provider1Suffix = "Dr.";
    var provider1IsVerified = "true";

    beforeEach('setup contract for each test', async function () {
        appInstance = await Provider.new()
    });

    it("Is Initialized empty",async function(){
        assert.equal(await appInstance.getProviderCount(), 0,'Providers are empty');

    });

    it("Add Provider", async function(){
        
        var reciept = await appInstance.addProvider(providerFirstName, providerLastName,providerSuffix,providerNpi, providerIsVerified);
        //Check Event
        assert.equal(reciept.logs.length, 1, 'Add Provider Event should be triggered');
        assert.equal(reciept.logs[0].event , "logProviderAdd", 'logProviderAdd Event should be triggered');
        assert.equal(reciept.logs[0].args.id , 1, 'Provider John Doe should have Id = 1');
        assert.equal(reciept.logs[0].args.npi , "111222333", 'Provider John Doe should have Npi = 111222333');
        assert.equal(reciept.logs[0].args.isVerified , true, 'Provider John Doe should have isVerified = true'); 
    });

    it("Add Multiple Provider", async function(){
        
        await appInstance.addProvider(providerFirstName, providerLastName,providerSuffix,providerNpi, providerIsVerified);
        await appInstance.addProvider(provider1FirstName, provider1LastName,provider1Suffix,provider1Npi, provider1IsVerified);
        
        var count = await appInstance.getProviderCount();
        assert.equal(count, 2, "Contract should have 2 providers");
    });

    it("Get Provider", async function(){
        
        await appInstance.addProvider(providerFirstName, providerLastName,providerSuffix,providerNpi, providerIsVerified);
        var count = await appInstance.getProviderCount();
        var result = await appInstance.getProvider(0);
        //pending asserts
    });


});