pragma solidity ^0.4.24;

contract Provider {

    //Complex types
    struct ProviderDataItem{
        uint id;
        address owner;
        string firstName;
        string lastName;
        string npi;
        string suffix;
        bool isVerified;

    }
    struct ProviderAddress {

        string addressLine1;
        string addressLine2;
        string city;
        string state;
        uint32 zipcode;
        bool isVerifiedLocation;
    }
   
    //State variables
    mapping(uint => ProviderDataItem) public providers;
    uint providerCount;

    //events
    event logProviderAdd(uint indexed id, address indexed sender, string npi, bool isVerified);

    function addProvider (string _firstName, string _lastName, string  _suffix, string _npi, bool _isVerified) 
    public
    {
        providerCount++;
        providers[providerCount] = ProviderDataItem(
            providerCount,
            msg.sender,
            _firstName,
            _lastName,
            _npi,
             _suffix,
            _isVerified
        );
        

        emit logProviderAdd(providerCount, msg.sender, _npi, _isVerified);
    }

    function getProviderCount() public view returns (uint) 
    {
        return providerCount;
    }

    function getProvider(uint id) public view returns (uint, address, string, string, string, string, bool) 
    {
        for(uint i = 0; i< providerCount; i++)
        {
            if(providers[i].id == id)
            {
                return (providers[i].id, providers[i].owner, providers[i].firstName, providers[i].lastName, providers[i].npi, providers[i].suffix, providers[i].isVerified);
            }

        }
    }

}