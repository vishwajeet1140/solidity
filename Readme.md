## Execution Steps.

Make sure you have Ganache and Truffle installed.

1. Clone the project.
2. run npm install.

Run below commands.

* truffle console --network ganache  
	+ If you dont have ganache follow below steps. 
		+ Rename truffle.js to anything so that truffle console does not read the truffle.js config
		+ truffle develop (cmd to use truffle-cli to create test ETH accounts).  
		
		
* migrate --compile-all --reset
* Provider.deployed().then(function(instance) {app = instance;})
* app.logProviderAdd({}, {}).watch(function(error,event){console.log(event);})
* app.addProvider("Doctor","One","Dr.","1231234","true",{from: web3.eth.accounts[1]})
* app.addProvider("Doctor","Two","Dr.","88734333","true",{from: web3.eth.accounts[1]})